define('clippy/stash', ['jquery', 'clippy'], function($, clippy) {
    $(function() {
        var $login = $('li.user-dropdown > a:contains("Log In")');
        if ($login.length) {
            var pos = $login.offset();
            clippy.load('Clippy', function(agent) {
                agent.show();
                agent.moveTo(pos.left - 70, pos.top + 70);
                agent.speak("Hey there!  It looks like you're not logged in.  Have you tried... logging in?");
                agent.play('GestureUp');
            });
        }
    });
});

require('clippy/stash');